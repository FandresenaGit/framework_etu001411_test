/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package servlet;

/**
 *
 * @author manan
 */
public class Association {
    private String classe;
    private String method;

    public Association() {
    }

    public Association(String classe, String method) {
        this.classe = classe;
        this.method = method;
    }

    public String getClasse() {
        return classe;
    }

    public String getMethod() {
        return method;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    public void setMethod(String method) {
        this.method = method;
    }
    
    
}
