/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package servlet;

import exception.UrlNotSupportedException;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.ModelView;
import util.Utilitaire;
import static util.Utilitaire.ajouterLesAssociations;
import static util.Utilitaire.upperCseFirst;

/**
 *
 * @author manan
 */
public class FrontServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public void init() throws ServletException {
        ServletContext context = this.getServletContext();
        HashMap<String, Association> hp = new HashMap<String, Association>();
        ajouterLesAssociations(hp);
        context.setAttribute("ctxt", hp);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, Exception {

        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String url = request.getRequestURI().toString();
            ServletContext context = this.getServletContext();
            HashMap<String, Association> hp = (HashMap<String, Association>) context.getAttribute("ctxt");
            Association assoc = new Utilitaire().checkUrl(url, hp);
            Class classe = Class.forName(assoc.getClasse());
            Object objClasse = classe.newInstance();
            Method[] m = objClasse.getClass().getDeclaredMethods();
            Field[] f = objClasse.getClass().getDeclaredFields();
            int nb = f.length;

            String ob = "";
            for (int i = 0; i < f.length; i++) {
                if(request.getParameter(f[i].getName()) == null){
                    break;
                }
                ob = request.getParameter(f[i].getName());
                String type = f[i].getType().getSimpleName();
                Method meth = objClasse.getClass().getDeclaredMethod("set" + upperCseFirst(f[i].getName()), f[i].getType());
                if (type.equalsIgnoreCase("String")) {
                    String a = ob;
                    meth.invoke(objClasse, a);
                } else if (type.equalsIgnoreCase("Integer")) {
                    Integer as = Integer.parseInt(ob);
                    meth.invoke(objClasse, as);
                }
            }

            ModelView objMethod = new ModelView();
            for (int i = 0; i < m.length; i++) {
                String meth = m[i].getName();
                if (meth.equals(assoc.getMethod())) {
                    objMethod = (ModelView) m[i].invoke(objClasse, null);
                }
            }
            request.setAttribute(objMethod.getUrl(), objMethod);
            RequestDispatcher dispat = request.getRequestDispatcher("/" + objMethod.getUrl());
            dispat.forward(request, response);

        } catch (UrlNotSupportedException use) {
            out.print(use.getMessage());
        } catch (Exception e) {
            out.print(e);
            
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(FrontServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(FrontServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
