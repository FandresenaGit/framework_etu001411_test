/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test;

import annotation.Url;
import java.util.HashMap;
import java.util.logging.Logger;
import model.ModelView;

/**
 *
 * @author manan
 */

public class Dept {
    
    Integer idDept;
    String nomDept;

    public Dept() {
    }

    public Dept(Integer idDept, String nomDept) {
        this.idDept = idDept;
        this.nomDept = nomDept;
    }

    public int getIdDept() {
        return idDept;
    }

    public String getNomDept() {
        return nomDept;
    }

    public void setIdDept(Integer idDept) {
        this.idDept = idDept;
    }

    public void setNomDept(String nomDept) {
        this.nomDept = nomDept;
    }
    
   @Url(nom="listDept.do")
   public ModelView listMode(){
       String[] a = {"1","2","3","4"};
       HashMap<String,String[]> hp = new HashMap<String,String[]>();
       hp.put("liste",a);
       ModelView mv = new ModelView();
       mv.setUrl("listDept.jsp");
       mv.setData(hp);
   
       return mv;
   }
   
   
   @Url(nom="ajoutDept.do")
   public ModelView ajoutMode(){
       ModelView mv = new ModelView();
       HashMap<String,String[]> hp = new HashMap<String,String[]>();
       mv.setUrl("ajoutDept.jsp");
       mv.setData(hp);
       return mv;
   }
   
   public void supprMode(){
   
   }
    
}
