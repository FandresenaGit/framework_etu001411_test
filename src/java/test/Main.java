/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test;

import java.util.HashMap;
import servlet.Association;
import static util.Utilitaire.ajouterLesAssociations;

/**
 *
 * @author manan
 */
public class Main {
   public static void main(String[] args){
       HashMap<String,Association> hp = new HashMap<String,Association>();
       ajouterLesAssociations(hp);
       for(HashMap.Entry m : hp.entrySet()){
           System.out.println("key : "+m.getKey());
       }
       System.out.print(hp.get("ajoutDept.do").getMethod());
   } 
}
