/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package util;

import annotation.Url;
import exception.UrlNotSupportedException;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import servlet.Association;

/**
 *
 * @author manan
 */
public class Utilitaire {

    public String retrieveUrlFromRawurl(String url) {
        String[] urlDecomposer = url.split("/");
        String urlRechercher = "";
        for (int i = 0; i < urlDecomposer.length; i++) {
            urlRechercher = urlDecomposer[i];
        }
        return urlRechercher;
    }

    public static Class<?>[] getClasses(String packageName) {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        String path = packageName.replace('.', '/');
        Enumeration<URL> resources = null;
        try {
            resources = classLoader.getResources(path);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        List<File> dirs = new ArrayList<>();
        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }
        List<Class<?>> classes = new ArrayList<>();
        dirs.forEach((directory) -> {
            classes.addAll(findClasses(directory, packageName));
        });
        return classes.toArray(new Class[classes.size()]);
    }

    private static List<Class<?>> findClasses(File directory, String packageName) {
        List<Class<?>> classes = new ArrayList<>();
        if (!directory.exists()) {
            return classes;
        }
        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file,
                        (!packageName.equals("") ? packageName + "." : packageName) + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                try {
                    classes.add(Class
                            .forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
                } catch (ClassNotFoundException e) {
                    System.err.println(e.getMessage());
                }
            }
        }
        return classes;
    }

    public static void ajouterLesAssociations(HashMap<String, Association> hp) {
        Class<?>[] a = getClasses("");

        for (int i = 0; i < a.length; i++) {
            Method[] fonction = a[i].getMethods();
            for (int i1 = 0; i1 < fonction.length; i1++) {
                Url hasAnnotation = fonction[i1].getAnnotation(Url.class);
                if (hasAnnotation != null) {
                    Association assoc = new Association();
                    assoc.setClasse(a[i].getName());
                    assoc.setMethod(fonction[i1].getName());
                    hp.put(hasAnnotation.nom(), assoc);
                }
            }
        }
    }

    public Association checkUrl(String url, HashMap<String, Association> hp) throws Exception {
        String vraiUrl = retrieveUrlFromRawurl(url);
        for (HashMap.Entry m : hp.entrySet()) {
            if (vraiUrl.equals((String) m.getKey())) {
                return hp.get(vraiUrl);
            }
        }
        throw new UrlNotSupportedException();
    }

    public static String upperCseFirst(String val) {
           char[] arr = val.toCharArray();
           arr[0]=Character.toUpperCase(arr[0]);
           return new String(arr);
    }
}
